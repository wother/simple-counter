/**
 * Wrapper for createHTMLElement.
 * @param {String} tag Valid HTML tag
 * @param {*} options Passthrough Options
 * @returns {HTMLElement}
 */
export function elt (tag, options) {
    let output = document.createElement(tag);

    if (options) {
        Object.keys(options).forEach(function (key){
            output[key] = options[key];
        });
    };

    return output;
};