/**
 * Wrapper for querySelector.
 * @param {String} selector 
 * @returns {HTMLElement}
 */
export function $$ (selector) {
    return document.querySelector(selector);
};

/**
 * Wrapper for querySelectorAll.
 * @param {String} selector 
 * @returns {HTMLCollection}
 */
export function $$$ (selector) {
    return document.querySelectorAll(selector);
};